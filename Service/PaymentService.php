<?php

namespace Aviatoo\Payment\Service;

use Aviatoo\Payment\Objects\CreditCard;
use Omnipay\Common\GatewayInterface;
use Omnipay\Omnipay;
use Stripe\Charge;
use Stripe\Source;
use Stripe\Stripe;
use Stripe\Token;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;


class PaymentService
{

    const SOFORT = "SOFORT";
    const CREDITCARD = "CREDITCARD";
    const GIROPAY = "GIROPAY";
    const PAYPAL = "PAYPAL";


    const PAYMENT_CURRENCY = "eur";
    const PAYMENT_DESCRIPTION = "Thanks for paying.";

    private $stripeKey, $paypalPw, $paypalUser, $paypalSignature,$request,$gateway;

    public function __construct(
        string $stripeKey = null,
        string $paypalPw = null,
        string $paypalUser = null,
        string $paypalSignature = null,
        RequestStack $requestStack){

        $this->request = $requestStack->getCurrentRequest();
        $this->stripeKey = $stripeKey;
        $this->paypalPw = $paypalPw;
        $this->paypalSignature = $paypalSignature;
        $this->paypalUser = $paypalUser;
        Stripe::setApiKey($this->stripeKey);
        $this->setPayPalCredentials();
    }

    private function setPayPalCredentials(){
        if($this->paypalPw && $this->paypalSignature && $this->paypalUser && $this->paypalPw){
            /**
             * @var GatewayInterface $this->gateway
             */
            $this->gateway = Omnipay::create('PayPal_Express');
            $this->gateway->setUsername($this->paypalUser);
            $this->gateway->setPassword($this->paypalPw);
            $this->gateway->setSignature($this->paypalSignature);
            $this->gateway->setTestMode(true); // set it to true when you develop and when you go to production to false
        }
    }

    /**
     * @param null $paymentType
     * @param $amount
     * @param string $description
     * @param CreditCard|null $creditCard
     * @return bool|null
     */
    public function createCharge($paymentType = null, $amount, $description = self::PAYMENT_DESCRIPTION, CreditCard $creditCard = null)
    {

        $params = [
            'amount' => $amount,
            'currency' => self::PAYMENT_CURRENCY,
            'description' => $description,
        ];

        switch ($paymentType) {
            case self::SOFORT:
                $query = $this->request->query;
                $params["source"] = $query->get('source', false);
                break;
            case self::GIROPAY:
                $query = $this->request->query;
                $params["source"] = $query->get('source', false);
                break;
            case self::PAYPAL:
                $query = $this->request->query;
                $params["token"] = $query->get('token', false);
                $params["playerid"] = $query->get('PlayerID', false);
                if(!$params["token"] && !$params["playerid"]) return false;
                return $this->gateway->completePurchase($params)->send();
            case self::CREDITCARD:
                if ($creditCard === null)   return false;
                $params['source'] = Token::create([
                    "card" => $creditCard->toArray()
                ]);
                break;
            default: return false;
        }
        try{
            return Charge::create([
                "amount" => $params["amount"],
                "currency" => self::PAYMENT_CURRENCY,
                "source" => $params["source"],
                "description" => "Payment"
            ]);
        }catch(\Exception $e){
//            dump($e);die;
        }
        return null;
    }

    public function getPaymentUrl($paymentType, $amount, $email, $returnUrl,$cancleUrl = false){

        $params = [
            "amount" => $amount,
            "currency" => self::PAYMENT_CURRENCY,
            "sofort" => [
                "country" => "DE"
            ],
            "redirect" => [
                "return_url" => $returnUrl
            ],
            "owner" => [
                "email" => $email,
                "name" => "succeeding_charge"
            ]
        ];
        switch ($paymentType){
            case self::PAYPAL:
                $response = $this->gateway->purchase([
                    'currency' => 'EUR',
                    'cancelUrl' => $cancleUrl,
                    'returnUrl' => $returnUrl, // in your case             //  you have registered in the routes 'payment_success'
                    'amount' => $amount,
                ])->send(); // here you send details to PayPal
                if ($response->isRedirect()) {
                    // redirect to offsite payment gateway
                    $res = $response->redirect();
                }
                else {
                    return false;
                }
                break;
            case self::GIROPAY:
                $params["type"] = strtolower(self::GIROPAY);
                break;
            case self::SOFORT:
                $params["type"] = strtolower(self::SOFORT);
                break;
            default: return false;
        }

        $source = Source::create($params);
        if(!$source) return false;
        $payment = array_combine($source->keys(),$source->values());
        return array_key_exists("redirect", $payment) ?
            array_combine($payment["redirect"]->keys(),$payment["redirect"]->values()) :
            false;
    }

}
